/*阅读系统--图书简介--文本展开*/
$(function() { //显示三行 多余隐藏 点击展开
	if($(".pHeight").height() <= 61) {
		$(".detail_txt_intro span.zhankai_quanbu").remove();
		$(".detail_txt_intro").css("pointer-events", "none");
	}
	$(".detail_txt_intro").click(function() {
		if($(this).children().hasClass("chaochu_shenglue")) {
			$(this).children().removeClass("chaochu_shenglue");
			$(this).children("span").html("");
		} else {
			$(this).children().addClass("chaochu_shenglue");
			$(this).children("span").html("展开全部");
		}
	})

	/*读后感--省略*/
	$(".bookItro_read_list_txt").click(function() {
		if($(this).children().hasClass("bookItro_read_shenglue")) {
			$(this).children().removeClass("bookItro_read_shenglue");
			$(this).children("span").html("");
		} else {
			$(this).children().addClass("bookItro_read_shenglue");
			$(this).children("span").html("展开全部");
		}
	});
});
/*点击事件*/
function changeColor(a) {
	var num = $(a).children("span")[0].innerHTML;
	if($(a).children("i").hasClass("activeGreen") == true) {
		$(a).children("i").removeClass("activeGreen");
		$(a).children("span").html(Math.abs(num) -1);
		
	} else {
		$(a).children("i").addClass("activeGreen");
		$(a).children("span").html(Math.abs(num)+1);
	}
}
/*收藏--点击事件*/
function changeCollect(a) {
	if($(a).children("i").hasClass("activeLove") == true) {
		$(a).children("i").removeClass("activeLove");
		$(".cancelCollect_popUp").css("display", "block");

	} else {
		$(a).children("i").addClass("activeLove");
	}
}
/*这里是测试的*/
$(".noRead").click(function() {
	$(this).css("display", "none");
	$(".hasRead").css("display", "block");
})

/*读后感点击出现弹窗*/
function popUp() {
	$(".noRead_popUp").css("display", "block");
}

function popUp1() {
	$(".deleteRead_popUp").css("display", "block");
}

function popUp3() {
	$(".planTo_popUp").css("display", "block");
}

function popUp4() {
	$(".start_popUp").css("display", "block");
	$(".start_popUp .popUpCon3 .fontBlue").css("color", "#1b80f1");
	$(".start_popUp .popUpCon3 .fontBlue").html("未参与测评");
	$(".start_popUp .popUpCon3 .popUpCon3_btn a.greenBg").html("开始测评");
}

function popUp5() {
	$(".start_popUp").css("display", "block");
	$(".start_popUp .popUpCon3 .popUpCon3_btn a.greenBg").html("再测一次");
	$(".start_popUp .popUpCon3 .fontBlue").html("未通过测评");
	$(".start_popUp .popUpCon3 .fontBlue").css("color", "#fd6951");
}

function popDown() {
	$(".noRead_popUp").css("display", "none");
	$(".deleteRead_popUp").css("display", "none");
	$(".planTo_popUp").css("display", "none");
	$(".start_popUp").css("display", "none");
	$(".cancelCollect_popUp").css("display", "none");
	$(".passed_popUp").css("display", "none");
	$(".pushOut_popUp").hide();
	$(".popSubmit_popUp").hide();
}

/*测评页面的弹窗*/
/*已通过*/
function passedPop() {
	$(".passed_popUp").css("display", "block");
}
/*未参与*/
/*popUp4*/
/*加入阅读任务*/
function addPlan() {
	$(".planTo_popUp").css("display", "block");
	$(".start_popUp").css("display", "none");
}
/*测评---计划时间弹窗*/
function addPlanYes() {
	$(".cepingInfo").show();
	$(".cepingInfo span").html("加入新任务！");
	$(".planTo_popUp").hide(1000);
	setTimeout(function() {
		$(".cepingInfo").hide();
	}, 2000); //持续2秒
}
/*未通过测评--弹窗*/
function noPass() {
	$(".start_popUp").show();
	$(".start_popUp .popUpCon3 .fontBlue").css("color", "#fd6951");
	$(".start_popUp .popUpCon3 .fontBlue").html("未通过测评");
	$(".start_popUp .popUpCon3 .popUpCon3_btn a.greenBg").html("再测一次");
}
/*6做题--页面*/
function pushOut() {
	$(".pushOut_popUp").show();
}
/*6做题--页面--提交弹窗*/
function popSubmit() {
	$(".popSubmit_popUp").show();
}