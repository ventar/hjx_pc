﻿function addLoadEvent(func) {
  var oldonload = window.onload;
  if (typeof window.onload != "function") {
    window.onload = func;
  } else {
    window.onload = function () {
      oldonload();
      func();
    }
  }
}

// 标签切换
function EW_tab(option) {
  this.oTab_btn = this.getDom(option.tabBtn);
  this.oTab_clist = this.getDom(option.tabCon);
  if (!this.oTab_btn || !this.oTab_clist) return;
  this.sCur = option.cur;
  this.type = option.type || 'click';
  this.nLen = this.oTab_btn.length;
  this.int()
}

EW_tab.prototype = {
  getId: function (id) {
    return document.getElementById(id)
  },
  getByClassName: function (className, parent) {
    var elem = [],
      node = parent != undefined && parent.nodeType == 1 ? parent.getElementsByTagName('*') : document.getElementsByTagName('*'),
      p = new RegExp("(^|\\s)" + className + "(\\s|$)");
    for (var n = 0, i = node.length; n < i; n++) {
      if (p.test(node[n].className)) {
        elem.push(node[n])
      }
    }
    return elem
  },
  getDom: function (s) {
    var nodeName = s.split(' '),
      p = this.getId(nodeName[0].slice(1)),
      c = this.getByClassName(nodeName[1].slice(1), p);
    if (!p || c.length == 0) return null;
    return c
  },
  change: function () {
    var cur = new RegExp(this.sCur, 'g');
    for (var n = 0; n < this.nLen; n++) {
      this.oTab_clist[n].style.display = 'none';
      this.oTab_btn[n].className = this.oTab_btn[n].className.replace(cur, '')
    }
  },
  int: function () {
    var that = this;
    this.oTab_btn[0].className += ' ' + this.sCur;
    this.oTab_clist[0].style.display = 'block';
    for (var n = 0; n < this.nLen; n++) {
      this.oTab_btn[n].index = n;
      this.oTab_btn[n]['on' + this.type] = function () {
        that.change();
        that.oTab_btn[this.index].className += ' ' + that.sCur;
        that.oTab_clist[this.index].style.display = 'block'
      }
    }
  }
}

// 头部下拉
$(".user a").click(function () {
  $(".user ul").toggleClass("is-show");
});
$(document).bind("click", function (e) {
  var target = $(e.target);
  if (target.closest(".user").length == 0) {
    $(".user ul").removeClass("is-show");
  }
});

// 排行榜下拉
$(".nav-list .tit a").click(function () {
  $(".nav-list .tit ul").toggleClass("is-show");
});
$(document).bind("click", function (e) {
  var target = $(e.target);
  if (target.closest(".nav-list .tit").length == 0) {
    $(".nav-list .tit ul").removeClass("is-show");
  }
});

// 首页图书遮罩
$(document).on("mouseenter", ".index .swiper-slide li", function () {
  $(this).children(".cover").show();
});
$(document).on("mouseleave", ".index .swiper-slide li .cover", function () {
  $(this).hide();
});

// 首页登录弹窗
function pupopen() {
  $("#bg").show();
  $("#popbox").show();
}

function pupopen1() {
  $("#bg").show();
  $("#popbox4").show();
}

function pupclose() {
  $("#bg").hide();
  $("#popbox").hide();
  $("#popbox2").hide();
  $("#popbox4").hide()
}

$("#popbox1 .confirm").click(function () {
  if ($("#grade").val() == "") {
    $(".tip1").css("opacity", "1");
    $(".tip2").css("opacity", "0");
  } else {
    if ($("#class").val() == "") {
      $(".tip1").css("opacity", "0");
      $(".tip2").css("opacity", "1");
    } else {
      $("#popbox1").hide()
      $("#popbox2").show();
    }
  }
})

$(".institution_option").click(function () {
  $(".login_nav dl.school_selects").addClass("is-show");
});
$(".grade").click(function () {
  $(".login_nav dl.grade_selects").addClass("is-show");
  $(".login_nav dl.class_selects").removeClass("is-show");
});
$(".class").click(function () {
  $(".login_nav dl.class_selects").addClass("is-show");
  $(".login_nav dl.grade_selects").removeClass("is-show");
});
$(".login_nav dl").click(function () {
  $(".login_nav dl").removeClass("is-show");
});
$("#popbox .school_selects a").click(function () {
  document.getElementById("institution_option").value = this.text;
})
$("#popbox1 .grade_selects a").click(function () {
  document.getElementById("grade").value = this.text;
})
$("#popbox1 .class_selects a").click(function () {
  document.getElementById("class").value = this.text;
})


// 答题页选择答案
$(".test li").click(function () {
  $(".test li").removeClass("current");
  $(this).addClass("current");
});